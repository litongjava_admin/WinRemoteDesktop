# WinRemoteDesktop
C# .Net Framework 4.0 开发的一款简易远程桌面管理工具
> 由于Windows自带远程桌面不带管理列表，所以开发此工具自用
- 可以管理远程桌面地址及账号密码及备注。
- 一键连接远程服务器，无需每次通过Windows自带远程桌面，而忘记账号、密码、IP等。
- 可同时连接多台远程服务器
- 托盘最小化

- 因为不支持分组管理 我已经放弃维护
![软件首页](https://gitee.com/litongjava_admin/WinRemoteDesktop/tree/master/Images/Main.png)
![添加服务器](https://gitee.com/litongjava_admin/WinRemoteDesktop/tree/master/Images/AddDesktop.png)